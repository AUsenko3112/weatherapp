import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsDayWeatherComponent } from './details-day-weather.component';

describe('DetailsDayWeatherComponent', () => {
  let component: DetailsDayWeatherComponent;
  let fixture: ComponentFixture<DetailsDayWeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsDayWeatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsDayWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
