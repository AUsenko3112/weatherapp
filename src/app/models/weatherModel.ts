export interface HourItemWeather {
  weatherIconUrl: string;
  temperature: string;
  pressure: string;
  humidity: string;
  windSpeed: string;
  rainProbability: number;
}

export class WeatherDayItemModel {
  minDayTemperature: string;
  maxDayTemperature: string;
  sunsetTime: string;
  sunriseTime: string;
  weatherGeneralIconUrl: string;
  hourlyWeather: {
    "0 hours": HourItemWeather;
    "3 hours": HourItemWeather;
    "6 hours": HourItemWeather;
    "9 hours": HourItemWeather;
    "12 hours": HourItemWeather;
    "15 hours": HourItemWeather;
    "18 hours": HourItemWeather;
    "21 hours": HourItemWeather;
  };

  constructor(data) {
    if (data) {
      this.minDayTemperature = data.minDayTemperature || null;
      this.maxDayTemperature = data.maxDayTemperature || null;
      this.sunsetTime = data.sunsetTime || null;
      this.sunriseTime = data.sunriseTime || null;
      this.weatherGeneralIconUrl = data.weatherGeneralIconUrl || null;
      this.hourlyWeather = data.hourlyWeather || null;
    }
  }
}
