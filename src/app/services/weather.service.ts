import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, Subject, Subscribable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {WeatherDayItemModel} from '../models/weatherModel';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  apiKeys = [
    {
      'apiName': 'WeatherApi',
      'key': 'f38493805e414e0cb8e84602201810'
    },
    {
      'apiName': 'OpenWeather',
      'key': '749dfc1d10e4d3be11b292193992712a'
    },
    {
      'apiName': 'WeatherBit RapidAPI',
      'key': '05ba703a50f6c5b26dd80e1a69c85c12'
    },
  ];

  constructor(
    private http: HttpClient
  ) {
  }

  getFirstApiWeatherData(city: string): Observable<any> {
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${this.apiKeys[1].key}`).pipe(
      map(data => {
        console.log('First weather API response', data);
        let response = this.makeFirstResponseToWeatherFormat(data);
        localStorage.setItem('firstAPIWeather', JSON.stringify(response)); // delete
        return response;
      }),
      catchError(err => {
        console.log('First weather API ERROR', err);
        return of(null);
      })
    );
  }

  getFirstWeatherHourlyWeather(data: [], arrayOfDataIndexes) {
    return {
      '0 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[0]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[0]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[0]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[0]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[0]]['pop']
      },
      '3 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[1]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[1]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[1]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[1]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[1]]['pop']
      },
      '6 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[2]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[2]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[2]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[2]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[2]]['pop']
      },
      '9 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[3]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[3]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[3]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[3]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[3]]['pop']
      },
      '12 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[4]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[4]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[4]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[4]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[4]]['pop']
      },
      '15 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[5]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[5]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[5]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[5]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[5]]['pop']
      },
      '18 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[6]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[6]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[6]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[6]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[6]]['pop']
      },
      '21 hours': {
        weatherIconUrl: '',
        temperature: Math.round(data[arrayOfDataIndexes[7]]['main']['temp'] - 273.15).toString(),
        pressure: data[arrayOfDataIndexes[7]]['main']['pressure'],
        humidity: data[arrayOfDataIndexes[7]]['main']['humidity'],
        windSpeed: data[arrayOfDataIndexes[7]]['wind']['speed'],
        rainProbability: data[arrayOfDataIndexes[7]]['pop']
      }
    };
  }

  makeFirstResponseToWeatherFormat(data): Array<WeatherDayItemModel> {
    return [
      {
        // countryCode: data.city.country,
        // city: data.city.name,
        minDayTemperature: this.getFirstWeatherMinDayTemperature(data.list.slice(0, 7)),
        maxDayTemperature: this.getFirstWeatherMaxDayTemperature(data.list.slice(0, 7)),
        sunsetTime: data.city.sunset,
        sunriseTime: data.city.sunrise,
        // dayOfTheWeek: moment().add(0, 'days').format('dddd'),
        // dayNumber: moment().add(0, 'days').format('D'),
        // month: moment().add(0, 'days').format('MMMM'),
        weatherGeneralIconUrl: '',
        hourlyWeather: this.getFirstWeatherHourlyWeather(data.list, [0, 1, 2, 3, 4, 5, 6, 7]),
      },
      {
        minDayTemperature: this.getFirstWeatherMinDayTemperature(data.list.slice(8, 15)),
        maxDayTemperature: this.getFirstWeatherMaxDayTemperature(data.list.slice(8, 15)),
        sunsetTime: data.city.sunset,
        sunriseTime: data.city.sunrise,
        weatherGeneralIconUrl: '',
        hourlyWeather: this.getFirstWeatherHourlyWeather(data.list, [8, 9, 10, 11, 12, 13, 14, 15]),
      },
      {
        minDayTemperature: this.getFirstWeatherMinDayTemperature(data.list.slice(16, 23)),
        maxDayTemperature: this.getFirstWeatherMaxDayTemperature(data.list.slice(16, 23)),
        sunsetTime: data.city.sunset,
        sunriseTime: data.city.sunrise,
        weatherGeneralIconUrl: '',
        hourlyWeather: this.getFirstWeatherHourlyWeather(data.list, [16, 17, 18, 19, 20, 21, 22, 23]),
      }
    ];
  }

  getFirstWeatherMinDayTemperature(temperaturesList): string {
    let minTemp = Math.round(Math.min.apply(null, temperaturesList.map(item => item.main.temp_min)) - 273.15).toString();
    return `${minTemp} °C`;
  }

  getFirstWeatherMaxDayTemperature(temperaturesList): string {
    let maxTemp = Math.round(Math.max.apply(null, temperaturesList.map(item => item.main.temp_max)) - 273.15).toString();
    return `${maxTemp} °C`;
  }

  // SECOND
  getSecondApiWeatherData(city: string): Observable<any> {
    return this.http.get(`http://api.weatherapi.com/v1/forecast.json?key=${this.apiKeys[0].key}&q=${city}&days=3`).pipe(
      map(data => {
        console.log('Second weather API response', data);
        let response = this.makeSecondResponseToWeatherFormat(data);
        localStorage.setItem('secondAPIWeather', JSON.stringify(response));
        return response;
      }),
      catchError(err => {
        console.log('Second weather API ERROR', err);
        return of(null);
      })
    );
  }

  makeSecondResponseToWeatherFormat(data): Array<WeatherDayItemModel> {
    return [
      {
        minDayTemperature: Math.round(data.forecast.forecastday[0].day.mintemp_c).toString(),
        maxDayTemperature: Math.round(data.forecast.forecastday[0].day.maxtemp_c).toString(),
        sunsetTime: data.forecast.forecastday[0].astro.sunset,
        sunriseTime: data.forecast.forecastday[0].astro.sunrise,
        weatherGeneralIconUrl: data.forecast.forecastday[0].day.condition.icon,
        hourlyWeather: this.getSecondApiDayHourlyWeather(data.forecast.forecastday[0].hour)
      },
      {
        minDayTemperature: Math.round(data.forecast.forecastday[1].day.mintemp_c).toString(),
        maxDayTemperature: Math.round(data.forecast.forecastday[1].day.maxtemp_c).toString(),
        sunsetTime: data.forecast.forecastday[1].astro.sunset,
        sunriseTime: data.forecast.forecastday[1].astro.sunrise,
        weatherGeneralIconUrl: data.forecast.forecastday[1].day.condition.icon,
        hourlyWeather: this.getSecondApiDayHourlyWeather(data.forecast.forecastday[1].hour)
      },
      {
        minDayTemperature: Math.round(data.forecast.forecastday[2].day.mintemp_c).toString(),
        maxDayTemperature: Math.round(data.forecast.forecastday[2].day.maxtemp_c).toString(),
        sunsetTime: data.forecast.forecastday[2].astro.sunset,
        sunriseTime: data.forecast.forecastday[2].astro.sunrise,
        weatherGeneralIconUrl: data.forecast.forecastday[2].day.condition.icon,
        hourlyWeather: this.getSecondApiDayHourlyWeather(data.forecast.forecastday[2].hour)
      }
    ];
  }

  getSecondApiDayHourlyWeather(hoursData) {
    return {
      '0 hours': {
        weatherIconUrl: hoursData[0].condition.icon,
        temperature: Math.round(hoursData[0].temp_c).toString(),
        pressure: hoursData[0].pressure_mb,
        humidity: hoursData[0].humidity,
        windSpeed: (hoursData[0].wind_mph / 3.556).toString(),
        rainProbability: hoursData[0].chance_of_snow || hoursData[0].chance_of_rain
      },
      '3 hours': {
        weatherIconUrl: hoursData[3].condition.icon,
        temperature: Math.round(hoursData[3].temp_c).toString(),
        pressure: hoursData[3].pressure_mb,
        humidity: hoursData[3].humidity,
        windSpeed: (hoursData[3].wind_mph / 3.556).toString(),
        rainProbability: hoursData[3].chance_of_snow || hoursData[3].chance_of_rain
      },
      '6 hours': {
        weatherIconUrl: hoursData[6].condition.icon,
        temperature: Math.round(hoursData[6].temp_c).toString(),
        pressure: hoursData[6].pressure_mb,
        humidity: hoursData[6].humidity,
        windSpeed: (hoursData[6].wind_mph / 3.556).toString(),
        rainProbability: hoursData[6].chance_of_snow || hoursData[6].chance_of_rain
      },
      '9 hours': {
        weatherIconUrl: hoursData[9].condition.icon,
        temperature: Math.round(hoursData[9].temp_c).toString(),
        pressure: hoursData[9].pressure_mb,
        humidity: hoursData[9].humidity,
        windSpeed: (hoursData[9].wind_mph / 3.556).toString(),
        rainProbability: hoursData[9].chance_of_snow || hoursData[9].chance_of_rain
      },
      '12 hours': {
        weatherIconUrl: hoursData[12].condition.icon,
        temperature: Math.round(hoursData[12].temp_c).toString(),
        pressure: hoursData[12].pressure_mb,
        humidity: hoursData[12].humidity,
        windSpeed: (hoursData[12].wind_mph / 3.556).toString(),
        rainProbability: hoursData[12].chance_of_snow || hoursData[12].chance_of_rain
      },
      '15 hours': {
        weatherIconUrl: hoursData[15].condition.icon,
        temperature: Math.round(hoursData[15].temp_c).toString(),
        pressure: hoursData[15].pressure_mb,
        humidity: hoursData[15].humidity,
        windSpeed: (hoursData[15].wind_mph / 3.556).toString(),
        rainProbability: hoursData[15].chance_of_snow || hoursData[15].chance_of_rain
      },
      '18 hours': {
        weatherIconUrl: hoursData[18].condition.icon,
        temperature: Math.round(hoursData[18].temp_c).toString(),
        pressure: hoursData[18].pressure_mb,
        humidity: hoursData[18].humidity,
        windSpeed: (hoursData[18].wind_mph / 3.556).toString(),
        rainProbability: hoursData[18].chance_of_snow || hoursData[18].chance_of_rain
      },
      '21 hours': {
        weatherIconUrl: hoursData[21].condition.icon,
        temperature: Math.round(hoursData[21].temp_c).toString(),
        pressure: hoursData[21].pressure_mb,
        humidity: hoursData[21].humidity,
        windSpeed: (hoursData[21].wind_mph / 3.556).toString(),
        rainProbability: hoursData[21].chance_of_snow || hoursData[21].chance_of_rain
      }
    };
  }

  //THIRD

  // getThirdAitWeatherData(city: string): Observable<any> {
  //   return this.http.get(`https://api.weatherbit.io/v2.0/forecast/hourly?city=${city}&key=${this.apiKeys[2].key}&hours=72`).pipe(
  //     map(data => {
  //       console.log('Third weather API response', data);
  //       let response = this.makeThirdResponseToWeatherFormat(data);
  //       localStorage.setItem('thirdAPIWeather', JSON.stringify(data)); // delete
  //       return response;
  //     }),
  //     catchError(err => {
  //       console.log('Third weather API ERROR', err);
  //       return of(null);
  //     })
  //   );
  // }

  // makeThirdResponseToWeatherFormat(data): Array<WeatherDayItemModel> {
  // makeThirdResponseToWeatherFormat(data) {
  //   console.log('y[1.1] Third weather API response', data);
  //   // return [
  //   //   {
  //   //     minDayTemperature: data
  //   //     // minDayTemperature: Math.round(data.forecast.forecastday[0].day.mintemp_c).toString(),
  //   //     // maxDayTemperature: Math.round(data.forecast.forecastday[0].day.maxtemp_c).toString(),
  //   //     // sunsetTime: data.forecast.forecastday[0].astro.sunset,
  //   //     // sunriseTime: data.forecast.forecastday[0].astro.sunrise,
  //   //     // weatherGeneralIconUrl: data.forecast.forecastday[0].day.condition.icon,
  //   //     // hourlyWeather: this.getSecondApiDayHourlyWeather(data.forecast.forecastday[0].hour)
  //   //   },
  //   // ];
  // }

  getGeneralWeatherData(weatherData1, weatherData2): Array<WeatherDayItemModel> {
    return [
      {
        minDayTemperature: weatherData1[0].minDayTemperature,
        maxDayTemperature: weatherData1[0].maxDayTemperature,
        sunsetTime: weatherData2[0].sunriseTime,
        sunriseTime: weatherData2[0].sunriseTime,
        weatherGeneralIconUrl: weatherData2[0].weatherGeneralIconUrl,
        hourlyWeather: this.getGeneralHourlyWeather(weatherData1[0].hourlyWeather, weatherData2[0].hourlyWeather),
      },
      {
        minDayTemperature: weatherData1[1].minDayTemperature,
        maxDayTemperature: weatherData1[1].maxDayTemperature,
        sunsetTime: weatherData2[1].sunriseTime,
        sunriseTime: weatherData2[1].sunriseTime,
        weatherGeneralIconUrl: weatherData2[1].weatherGeneralIconUrl,
        hourlyWeather: this.getGeneralHourlyWeather(weatherData1[1].hourlyWeather, weatherData2[1].hourlyWeather),
      },
      {
        minDayTemperature: weatherData1[2].minDayTemperature,
        maxDayTemperature: weatherData1[2].maxDayTemperature,
        sunsetTime: weatherData2[2].sunriseTime,
        sunriseTime: weatherData2[2].sunriseTime,
        weatherGeneralIconUrl: weatherData2[2].weatherGeneralIconUrl,
        hourlyWeather: this.getGeneralHourlyWeather(weatherData1[2].hourlyWeather, weatherData2[2].hourlyWeather),
      },
    ];
  }

  getGeneralHourlyWeather(firstWeatherHours, secondWeatherHours) {
    return {
      '0 hours': {
        weatherIconUrl: secondWeatherHours['0 hours'].weatherIconUrl,
        temperature: firstWeatherHours['0 hours'].temperature,
        pressure: (secondWeatherHours['0 hours'].pressure - 350).toString(),
        humidity: secondWeatherHours['0 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['0 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['0 hours'].rainProbability
      },
      '3 hours': {
        weatherIconUrl: secondWeatherHours['3 hours'].weatherIconUrl,
        temperature: firstWeatherHours['3 hours'].temperature,
        pressure: (secondWeatherHours['3 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['3 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['3 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['3 hours'].rainProbability
      },
      '6 hours': {
        weatherIconUrl: secondWeatherHours['6 hours'].weatherIconUrl,
        temperature: firstWeatherHours['6 hours'].temperature,
        pressure: (secondWeatherHours['6 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['6 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['6 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['6 hours'].rainProbability
      },
      '9 hours': {
        weatherIconUrl: secondWeatherHours['9 hours'].weatherIconUrl,
        temperature: firstWeatherHours['9 hours'].temperature,
        pressure: (secondWeatherHours['9 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['9 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['9 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['9 hours'].rainProbability
      },
      '12 hours': {
        weatherIconUrl: secondWeatherHours['12 hours'].weatherIconUrl,
        temperature: firstWeatherHours['12 hours'].temperature,
        pressure: (secondWeatherHours['12 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['12 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['12 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['12 hours'].rainProbability
      },
      '15 hours': {
        weatherIconUrl: secondWeatherHours['15 hours'].weatherIconUrl,
        temperature: firstWeatherHours['15 hours'].temperature,
        pressure: (secondWeatherHours['15 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['15 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['15 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['15 hours'].rainProbability
      },
      '18 hours': {
        weatherIconUrl: secondWeatherHours['18 hours'].weatherIconUrl,
        temperature: firstWeatherHours['18 hours'].temperature,
        pressure: (secondWeatherHours['18 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['18 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['18 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['18 hours'].rainProbability
      },
      '21 hours': {
        weatherIconUrl: secondWeatherHours['21 hours'].weatherIconUrl,
        temperature: firstWeatherHours['21 hours'].temperature,
        pressure: (secondWeatherHours['21 hours'].pressure - 250).toString(),
        humidity: secondWeatherHours['21 hours'].humidity,
        windSpeed: (Math.round(secondWeatherHours['21 hours'].windSpeed * 100 / 3.66) / 100).toString(),
        rainProbability: secondWeatherHours['21 hours'].rainProbability
      },
    };
  }
}
